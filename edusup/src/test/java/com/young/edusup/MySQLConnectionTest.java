package com.young.edusup;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class MySQLConnectionTest {
	private static final String DRIVER =
			"com.mysql.cj.jdbc.Driver";
	private static final String URL =
			"jdbc:mysql://192.168.5.128:32768/edusup";
	private static final String USER =
			"edusup";
	private static final String PW =
			"Asdf!234";
	
	@Test
	public void testConnection() throws Exception{
		Class.forName(DRIVER);
		
		try(Connection con = DriverManager.getConnection(URL, USER, PW)){
			System.out.println(con);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
