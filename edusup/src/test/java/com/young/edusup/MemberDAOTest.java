package com.young.edusup;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.young.domain.MemberVO;
import org.young.persistence.MemberDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class MemberDAOTest {

	@Inject
	private MemberDAO dao;
	
	@Test
	public void testTime() throws Exception{
		System.out.println(dao.getTime());
	}
	
	@Test
	public void testInsertMember() throws Exception{
		MemberVO vo = new MemberVO();
		vo.setUserid("user05");
		vo.setUserpw("user05");
		vo.setUsername("USER05");
		vo.setEmail("user05@aaa.com");
		vo.setPhone("000-000-0005");
		
		dao.insertMember(vo);
	}
	
	@Test
	public void testSelectMember() throws Exception{
		dao.readMember("user02");
	}
	
	@Test
	public void testReadWithPW() throws Exception{
		dao.readWithPW("user03", "user03");
	}
}
